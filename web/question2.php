<?php 
    if(!empty($_COOKIE)) {
        $data1 = json_decode($_COOKIE['arr_answer'], true);    
        if(!empty($_POST)) {
            $answer_6 = $_POST['answer_6'];
            $answer_7 = $_POST['answer_7'];
            $answer_8 = $_POST['answer_8'];
            $answer_9 = $_POST['answer_9'];
            $answer_10 = $_POST['answer_10'];
    
            $arr_answer_2 = array($answer_6,$answer_7,$answer_8,$answer_9,$answer_10);
            
            $list_answer = array_merge($data1,$arr_answer_2);
            
            setcookie('list_answer',json_encode($list_answer),time() + (86400 * 30), "/" );
            
            header('Location: finish_exam.php');
            die();
        }
    }
   
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    .question_answer {
        display: flex;
        justify-content: space-around;
    }

    .question {
        border-top: 1px solid #ccc;
        height: 20%;
        margin-top: 10px;
    }

    .question_text {
        margin: 0;
        font-size: 30px;

    }

    .question_answer-option {
        display: flex;
        height: 30px;
        font-size: 17px;
        margin: 0;
        text-align: center;
    }

    .answer {
        margin: 0;
        line-height: 30px;
        font-size: 22px;
    }

    .btn_next {
        background-color: #4dc3e8;
        border: none;
        padding: 10px 20px;
        border-radius: 4px;
        color: #fff;
        font-size: 20px;
    }

    .answer-boder {
        border-bottom: 1px solid #ccc;

    }
    </style>

</head>

<body>
    <form method="post">
        <header style=" display: flex; justify-content: center; font-size: 25px; margin: 10px 0 30px 0;">
            Hãy trả lời các câu hỏi dưới đây
        </header>
        <div>
            <center>
                <div class="question">
                    <h1 class="question_text">Day la cau hoi 6 </h1>
                    <div class="question_answer">
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_6" value="A"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    A. Đáp án 1
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_6" value="B"></input>
                                <p class="answer" style="min-width: 100px">
                                    B. Đáp án 2
                                </p>
                            </h2>
                        </div>
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_6" value="C"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    C. Đáp án 3
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_6" value="D"></input>
                                <p class="answer" style="min-width: 100px">
                                    D. Đáp án 4
                                </p>
                            </h2>

                        </div>
                    </div>
                </div>
                <div class="question">
                    <h1 class="question_text">Day la cau hoi 7 </h1>
                    <div class="question_answer">
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_7" value="A"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    A. Đáp án 1
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_7" value="B"></input>
                                <p class="answer" style="min-width: 100px">
                                    B. Đáp án 2
                                </p>
                            </h2>
                        </div>
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_7" value="C"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    C. Đáp án 3
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_7" value="D"></input>
                                <p class="answer" style="min-width: 100px">
                                    D. Đáp án 4
                                </p>
                            </h2>

                        </div>
                    </div>
                </div>
                <div class="question">
                    <h1 class="question_text">Day la cau hoi 8 </h1>
                    <div class="question_answer">
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_8" value="A"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    A. Đáp án 1
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_8" value="B"></input>
                                <p class="answer" style="min-width: 100px">
                                    B. Đáp án 2
                                </p>
                            </h2>
                        </div>
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_8" value="C"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    C. Đáp án 3
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_8" value="D"></input>
                                <p class="answer" style="min-width: 100px">
                                    D. Đáp án 4
                                </p>
                            </h2>

                        </div>
                    </div>
                </div>
                <div class="question">
                    <h1 class="question_text">Day la cau hoi 9 </h1>
                    <div class="question_answer">
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_9" value="A"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    A. Đáp án 1
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_9" value="B"></input>
                                <p class="answer" style="min-width: 100px">
                                    B. Đáp án 2
                                </p>
                            </h2>
                        </div>
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_9" value="C"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    C. Đáp án 3
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_9" value="D"></input>
                                <p class="answer" style="min-width: 100px">
                                    D. Đáp án 4
                                </p>
                            </h2>

                        </div>
                    </div>
                </div>
                <div class="question">
                    <h1 class="question_text">Day la cau hoi 10 </h1>
                    <div class="question_answer">
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_10" value="A"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    A. Đáp án 1
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_10" value="B"></input>
                                <p class="answer" style="min-width: 100px">
                                    B. Đáp án 2
                                </p>
                            </h2>
                        </div>
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_10" value="C"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    C. Đáp án 3
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_10" value="D"></input>
                                <p class="answer" style="min-width: 100px">
                                    D. Đáp án 4
                                </p>
                            </h2>

                        </div>
                    </div>
                </div>
            </center>
        </div>
        <footer style="display: flex; justify-content: center; margin-top: 20px;">
            <button class="btn_next">Nộp bài</button>
        </footer>
    </form>
</body>

</html>