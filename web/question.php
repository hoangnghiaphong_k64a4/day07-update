<?php 
    if(!empty($_POST)) {
        $answer_1 = $_POST['answer_1'];
        $answer_2 = $_POST['answer_2'];
        $answer_3 = $_POST['answer_3'];
        $answer_4 = $_POST['answer_4'];
        $answer_5 = $_POST['answer_5'];

        $arr_answer = array($answer_1,$answer_2,$answer_3,$answer_4,$answer_5);
      
        setcookie('arr_answer',json_encode($arr_answer),time() + (86400 * 30), "/" );
        
        header('Location: question2.php');
        die();
        
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    .question_answer {
        display: flex;
        justify-content: space-around;
    }

    .question {
        border-top: 1px solid #ccc;
        height: 20%;
        margin-top: 10px;
    }

    .question_text {
        margin: 0;
        font-size: 30px;

    }

    .question_answer-option {
        display: flex;
        height: 30px;
        font-size: 17px;
        margin: 0;
        text-align: center;
    }

    .answer {
        margin: 0;
        line-height: 30px;
        font-size: 22px;
    }

    .btn_next {
        background-color: #4dc3e8;
        border: none;
        padding: 10px 20px;
        border-radius: 4px;
        color: #fff;
        font-size: 20px;
    }

    .answer-boder {
        border-bottom: 1px solid #ccc;

    }
    </style>

</head>

<body>
    <form method="post">
        <header style=" display: flex; justify-content: center; font-size: 25px; margin: 10px 0 30px 0;">
            Hãy trả lời các câu hỏi dưới đây
        </header>
        <div>
            <center>
                <div class="question">
                    <h1 class="question_text">Day la cau hoi 1 </h1>
                    <div class="question_answer">
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_1" value="A"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    A. Đáp án 1
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_1" value="B"></input>
                                <p class="answer" style="min-width: 100px">
                                    B. Đáp án 2
                                </p>
                            </h2>
                        </div>
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_1" value="C"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    C. Đáp án 3
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_1" value="D"></input>
                                <p class="answer" style="min-width: 100px">
                                    D. Đáp án 4
                                </p>
                            </h2>

                        </div>
                    </div>
                </div>
                <div class="question">
                    <h1 class="question_text">Day la cau hoi 2 </h1>
                    <div class="question_answer">
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_2" value="A"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    A. Đáp án 1
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_2" value="B"></input>
                                <p class="answer" style="min-width: 100px">
                                    B. Đáp án 2
                                </p>
                            </h2>
                        </div>
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_2" value="C"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    C. Đáp án 3
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_2" value="D"></input>
                                <p class="answer" style="min-width: 100px">
                                    D. Đáp án 4
                                </p>
                            </h2>

                        </div>
                    </div>
                </div>
                <div class="question">
                    <h1 class="question_text">Day la cau hoi 3 </h1>
                    <div class="question_answer">
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_3" value="A"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    A. Đáp án 1
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_3" value="B"></input>
                                <p class="answer" style="min-width: 100px">
                                    B. Đáp án 2
                                </p>
                            </h2>
                        </div>
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_3" value="C"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    C. Đáp án 3
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_3" value="D"></input>
                                <p class="answer" style="min-width: 100px">
                                    D. Đáp án 4
                                </p>
                            </h2>

                        </div>
                    </div>
                </div>
                <div class="question">
                    <h1 class="question_text">Day la cau hoi 4 </h1>
                    <div class="question_answer">
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_4" value="A"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    A. Đáp án 1
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_4" value="B"></input>
                                <p class="answer" style="min-width: 100px">
                                    B. Đáp án 2
                                </p>
                            </h2>
                        </div>
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_4" value="C"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    C. Đáp án 3
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_4" value="D"></input>
                                <p class="answer" style="min-width: 100px">
                                    D. Đáp án 4
                                </p>
                            </h2>

                        </div>
                    </div>
                </div>
                <div class="question">
                    <h1 class="question_text">Day la cau hoi 5 </h1>
                    <div class="question_answer">
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_5" value="A"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    A. Đáp án 1
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_5" value="B"></input>
                                <p class="answer" style="min-width: 100px">
                                    B. Đáp án 2
                                </p>
                            </h2>
                        </div>
                        <div>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_5" value="C"></input>
                                <p class="answer answer-boder" style="min-width: 100px">
                                    C. Đáp án 3
                                </p>
                            </h2>
                            <h2 class="question_answer-option">
                                <input type="radio" name="answer_5" value="D"></input>
                                <p class="answer" style="min-width: 100px">
                                    D. Đáp án 4
                                </p>
                            </h2>

                        </div>
                    </div>
                </div>
            </center>
        </div>
        <footer style="display: flex; justify-content: center; margin-top: 20px;">
            <button class="btn_next">Next</button>
        </footer>
    </form>
</body>

</html>