<?php 
    session_start();
    $spe = ['MAT' => "Khoa học máy tính", 'KDL' => "Khoa học vật liệu"];
    $department = $keys ='';
    if(!empty($_POST)) {
        if(isset($_POST['department'])) {
            $department = $_POST['department'];

        }
        if(isset($_POST['keys'])) {
            $keys = $_POST['keys'];
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    .number_student {
        margin: 0;
    }

    .btn_search,
    .btn_delete {
        justify-content: center;
        margin-left: 30px;
    }

    .label {
        display: flex;
    }

    .btn_search-btn,
    .btn_delete-btn {
        padding: 8px 16px;
        color: #fff;
        border-radius: 4px;
        background-color: #1345c2;
        border: 2px solid #0e118f;
    }

    .btn_search-add {
        padding: 4px 16px;
        color: #fff;
        border-radius: 4px;
        background-color: #1345c2;
        border: 2px solid #0e118f;
    }

    .table {
        width: 100%;
        height: 100%;
        left: 0;
        top: 0;
    }


    td {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 4px;
        height: 30px;
    }

    .table_title {
        text-align: left;
    }

    .table_td-btn {
        background-color: #aed0f2;
        border: 2px solid #809ee2;
        color: #fff;
        padding: 0;
        height: 30px;
        width: 40px;
        padding: 6px;
        font-size: 13px;
        cursor: pointer;
    }

    .input_keys {
        height: 26px;
        padding: 0;
    }

    .select_department,
    label {
        height: 30px;
    }
    </style>
</head>

<body>
    <div>
        <div style="width:30%; margin: auto; ">
            <form method="post">
                <div style="display: flex; margin-bottom: 10px;">
                    <label class="label" style="flex:1; text-align: center;">
                        <span>
                            Khoa
                        </span>
                    </label>
                    <select class="select_department" name="department" style="border: 2px solid #0b67ad ;flex:2">
                        <option selected></option>;
                        <?php
                                
                                foreach($spe as $key => $value) {
                                    if($department == $key) {
                                        echo '<option value="'.$key.'" selected >'.$value.'</option>';
                                    } else {
                                        echo '<option value="'.$key.'"  >'.$value.'</option>';
                                    }
                                }
                            
                        ?>
                    </select>
                </div>
                <div style="display: flex;">
                    <label class="label" style="flex:1; text-align: center;">
                        <span>
                            Từ khóa
                        </span>
                    </label>
                    <input class="input_keys" style="flex:2 ;" name="keys" value="<?=$keys?>">
                </div>
                <div style="display:flex; justify-content: center; margin-top: 10px;">
                    <span class="btn_delete">
                        <button class="btn_delete-btn" type="button" onclick="Delete()">Xóa</button>
                    </span>
                    <span class="btn_search">
                        <button class="btn_search-btn">Tim kiếm</button>
                    </span>
                </div>
            </form>
        </div>
        <form action="day06.php">
            <div style="display: flex;width:30%; justify-content: space-between; margin-left: 60px; ">
                <p class="number_student">Số sinh viên tìm thấy</p>
                <button class="btn_search-add" onclick="Redirect()">Thêm</button>
            </div>
        </form>
        <div style=" padding: 10px 60px 0 60px;">
            <table class="table">
                <thead>
                    <tr>
                        <th class="table_title" style="width: 10%;">No</th>
                        <th class="table_title" style="width: 35%;">Tên sinh viên</th>
                        <th class="table_title" style="width: 35%;">Khoa</th>
                        <th class="table_title" style="width: 20%;">Action</th>
                    </tr>
                    <tr>
                        <td class="table_td">1</td>
                        <td class="table_td">Nguyễn Văn A</td>
                        <td class="table_td">Khoa học máy tính</td>
                        <td class="table_td">
                            <button class="table_td-btn">Xóa</button>
                            <button class="table_td-btn">Sửa</button>
                        </td>
                    </tr>
                    <tr>
                        <td class="table_td">1</td>
                        <td class="table_td">Nguyễn Văn A</td>
                        <td class="table_td">Khoa học máy tính</td>
                        <td class="table_td">
                            <button class="table_td-btn">Xóa</button>
                            <button class="table_td-btn">Sửa</button>
                        </td>
                    </tr>
                    <tr>
                        <td class="table_td">1</td>
                        <td class="table_td">Nguyễn Văn A</td>
                        <td class="table_td">Khoa học máy tính</td>
                        <td class="table_td">
                            <button class="table_td-btn">Xóa</button>
                            <button class="table_td-btn">Sửa</button>
                        </td>
                    </tr>
                    <tr>
                        <td class="table_td">1</td>
                        <td class="table_td">Nguyễn Văn A</td>
                        <td class="table_td">Khoa học máy tính</td>
                        <td class="table_td">
                            <button class="table_td-btn">Xóa</button>
                            <button class="table_td-btn">Sửa</button>
                        </td>
                    </tr>

                </thead>
            </table>
        </div>
    </div>
</body>
<script>
var departmentInput = document.querySelector('.select_department');
var keysInput = document.querySelector('.input_keys');

function Delete() {
    departmentInput.value = "";
    keysInput.value = "";
}
</script>

</html>