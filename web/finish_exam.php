<?php 

    $result = array("A","A","A","A","A","B","B","B","B","B");

    if(!empty($_COOKIE)) {
        $data = json_decode($_COOKIE['list_answer'], true);
    }
    $arr_answer_correct = array_intersect_assoc($result,$data);
    $point = sizeof($arr_answer_correct);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    .result {
        display: flex;
        margin: auto;
        width: 50%;
        border: 3px solid green;
        padding: 10px;
        align-items: center;
    }

    .notification {
        color: red;
    }
    </style>

</head>

<body>
    <div class="result">
        <div>
            <?php 
            echo '<h1>'.'Điểm bài kiểm tra của bạn:'.' '.$point.' điểm'.'</h1>';
            if($point < 4) {
                echo '<h1 class="notification">'.'Bạn quá kém, cần luyện tập thêm'.'</h1>';
            } elseif(4 <= $point && $point <= 7) {
                echo '<h1 class="notification">'.'Cũng bình thường'.'</h1>';
            } else {
                echo '<h1 class="notification">'.'Sắp thành trợ giảng lớp PHP'.'</h1>';
            }
         

            ?>
        </div>
    </div>
</body>

</html>